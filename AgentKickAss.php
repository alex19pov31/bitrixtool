<?php

namespace BitrixTool;

/**
* 
*/
class AgentKickAss
{
	const TBL_NAME = 'b_agent';
	private $_connection;
	private $_listAgents;
	
	function __construct()
	{	
		$this->_connection = \Bitrix\Main\Application::getConnection();
		//$this->_listAgents = $this->getListAgents();
	}

	public function run()
	{
		$arAgents = $this->getListAgents();

		foreach ($arAgents as $agent) {
			$this->setRunningStatus($agent);
			$this->runAgent($agent);
			$this->finishAgent($agent);
		}
	}

	public function getListAgents()
	{
		if($this->_listAgents) return $this->_listAgents;

		$connection = $this->_connection;
		$sql = "SELECT * FROM `".self::TBL_NAME."` WHERE `IS_PERIOD`='Y' AND `NEXT_EXEC` <= NOW() AND `RUNNING`='N'";
		$arAgents = array();

		try {
			$res = $connection->query($sql);
			while($arAgent = $res->fetch()){
				$arAgents[] = $arAgent;
			}
		} catch(Exception $e){
			return false;
		}

		$this->_listAgents = $arAgents;
		return $this->_listAgents;
	}

	private function setRunningStatus($arAgent)
	{
		$connection = $this->_connection;
		$sql = "UPDATE `".self::TBL_NAME."` SET `RUNNING`='Y' WHERE `ID`={$arAgent['ID']}";
		try {
			$res = $connection->queryExecute($sql);
		} catch(Exception $e){
			return false;
		}

		return true;
	}

	private function finishAgent($arAgent)
	{
		$connection = $this->_connection;
		$time = strtotime($arAgent['NEXT_EXEC']) + $arAgent['AGENT_INTERVAL'];
		$date = date('Y-m-d H:i:s', $time);
		$sql = "UPDATE `".self::TBL_NAME."` 
		SET `RUNNING`='N', `LAST_EXEC`=NOW(), 
		`NEXT_EXEC`=FROM_UNIXTIME(UNIX_TIMESTAMP(NOW())+`AGENT_INTERVAL`) 
		WHERE `ID`={$arAgent['ID']}";
		try {
			$res = $connection->queryExecute($sql);
		} catch(Exception $e){
			return false;
		}

		return true;
	}

	private function runAgent($arAgent)
	{
		if(!CModule::IncludeModule($arAgent['MODULE_ID'])) return false;
		return eval($arAgent['NAME']);
	}
}